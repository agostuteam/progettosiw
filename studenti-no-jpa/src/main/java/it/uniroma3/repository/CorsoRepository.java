package it.uniroma3.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;

public interface CorsoRepository extends CrudRepository<Corso,Long> {
	
	public Optional<Corso> findById(Long id);
	
	public List<Corso> findByNome(String nome);
	
	public List<Corso> findByCentro(Centro centro);
	
	public List<Corso> findAll();
	
	public List<Corso> findByStudenti_id(Long id);
	
	
	
	
}
