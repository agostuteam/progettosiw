package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Studente;



public interface StudenteRepository extends CrudRepository<Studente, Long> {

	
	public Optional<Studente> findById(Long id);
	
	public List<Studente> findAll();
	
	public List<Studente> findByNome(String nome);
	
	public List<Studente> findByCognome(String cognome);
	
	public List<Studente> findByEmail(String email);
	
	public List<Studente> findByCorsi_Id(Long id);

}
