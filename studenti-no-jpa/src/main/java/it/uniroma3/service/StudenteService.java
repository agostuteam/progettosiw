package it.uniroma3.service;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Studente;
import it.uniroma3.repository.CorsoRepository;
import it.uniroma3.repository.StudenteRepository;

@Transactional
@Service
public class StudenteService {
	
	@Autowired
	public StudenteRepository studenteRepository;
	
	@Autowired
	public CorsoRepository corsoRepository;
	
	public Studente save(Studente s) {
		s.setNome(s.getNome().toUpperCase());
		s.setCognome(s.getCognome().toUpperCase());
		return this.studenteRepository.save(s);
	}
	
	public List<Studente> findAll(){
		return this.studenteRepository.findAll();
	}
	
	public List<Studente> findbyNome(String nome){
		return this.studenteRepository.findByNome(nome);
	}
	
	public List<Studente> findbyCognome(String nome){
		return this.studenteRepository.findByCognome(nome);
	}
	
	public List<Studente> findByEmail(String email){
		return this.studenteRepository.findByEmail(email);
	}
	
	public List<Studente> findByCorso(Long id){
		return this.studenteRepository.findByCorsi_Id(id);
	}

	public boolean alreadyExists(Studente studente) {
		List<Studente> studenti = this.studenteRepository.findByEmail(studente.getEmail());
		if (studenti.size() > 0)
			return true;
		else 
			return false;
	}	
	
	public Studente findById(Long id) {
		Optional<Studente> s = this.studenteRepository.findById(id);
		if(s.isPresent())
			return s.get();
		else
			return null;
	}

	
	

}
