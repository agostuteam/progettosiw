package it.uniroma3;
import java.sql.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Studente;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;
import it.uniroma3.service.StudenteService;

@EnableAutoConfiguration(exclude={ErrorMvcAutoConfiguration.class})
@SpringBootApplication
public class studentiNoSpringApp {
	
	
	@Autowired
	private StudenteService studenteService;
	
	@Autowired
	private ResponsabileService responsabileService;
	
	@Autowired
	private CentroService centroService;
	
	@Autowired
	private CorsoService corsoService;
	
	public static void main(String[] args) {
		SpringApplication.run(studentiNoSpringApp.class, args);
	}
	

	@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
	
	@PostConstruct
	public void init() {
		
		BCryptPasswordEncoder b = bCryptPasswordEncoder();
		//Salvo centro senza riferimento al responsabile (poichè nel db ancora non esiste)
		Centro c1= new Centro("CentroEstetico", "Email", "Via Aldo baldovinetti,Roma", "367676776", 20);
		centroService.save(c1);
		//Salvo il responsabile con centro assegnato
		Responsabile d = new Responsabile("ago@ago",b.encode("password"),"DIRETTORE");
		Responsabile r = new Responsabile("ale@ale",b.encode("password"),"RESPONSABILE");
		responsabileService.save(d);
		r.setCentro(c1);
		responsabileService.save(r);
		//Setto responsabile (che ora esiste nel db) e aggiorno il centro
		c1.setResponsabile(r);
		
		centroService.save(c1);
		
		//Salvo allievi senza riferimenti a attività (poichè nel db non esistono)
		Studente al1 = new Studente("marco","rossi","ciao@ciao.com", new Date(2));
		Studente al2 = new Studente("tommi","zazza","tomm@zazz.com", new Date(2));
		Studente al3 = new Studente("GIOO","pieffe","gioo@zazz.com", new Date(2));
		studenteService.save(al1);
		studenteService.save(al2);
		studenteService.save(al3);
		//Salvo Attivita con centro  e allievi assegnati
		Corso a1 = new Corso("ciao", new Date(1));
		a1.setCentro(c1);
		a1.getStudenti().add(al1);
		corsoService.save(a1);
		
	}

}
