package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Centro {
	
	@Id	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(nullable=false)
	private String nome;
	@Column(nullable=false)
	private String email;
	@Column(nullable=false)
	private String indirizzo;
	@Column(nullable=false)
	private String telefono;
	@Column(nullable=false)
	private int capienzaMax;
	@OneToOne
	private Responsabile responsabile;
	
	@OneToMany(mappedBy="centro")
	private List<Corso> corsi;
	
	
	
	public Centro(){
		
	this.corsi = new ArrayList<Corso>();

		
	}
	
	public Centro(String nome,String email,String indirizzo,String telefono,int cap)
	{
		
		this.nome = nome;
		this.email = email;
		this.indirizzo = indirizzo;
		this.telefono = telefono;
		this.capienzaMax = cap;
		this.responsabile = null;
		this.corsi = new ArrayList<Corso>();
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getCapienzaMax() {
		return capienzaMax;
	}

	public void setCapienzaMax(int capienzaMax) {
		this.capienzaMax = capienzaMax;
	}

	public Responsabile getResponsabile() {
		return responsabile;
	}

	public void setResponsabile(Responsabile responsabile) {
		this.responsabile = responsabile;
	}

	public List<Corso> getCorsi() {
		return corsi;
	}

	public void setCorsi(List<Corso> corsi) {
		this.corsi = corsi;
	}

	
}
