package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Corso;

@Component
public class CorsoValidator implements Validator {

	
	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required","campo obbligatorio!");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "data", "required","campo obbligatorio!");
       
	}

	@Override
	public boolean supports(Class<?> clazz) {
		
		return Corso.class.equals(clazz);
	}

}
