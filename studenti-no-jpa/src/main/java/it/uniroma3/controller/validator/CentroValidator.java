package it.uniroma3.controller.validator;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Centro;

@Component
public class CentroValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Centro.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required","Campo obbligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indirizzo", "required","Campo obbligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "telefono", "required","Campo obbligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "capienzaMax", "required","Campo obbligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required","Campo obbligatorio");
	}

}
