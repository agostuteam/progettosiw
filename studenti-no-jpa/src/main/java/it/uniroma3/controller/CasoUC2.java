package it.uniroma3.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.controller.validator.StudenteValidator;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Studente;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;
import it.uniroma3.service.StudenteService;


@Controller
public class CasoUC2 {

	@Autowired
	private CorsoService corsoService;

	@Autowired
	private StudenteService studenteService;
	@Autowired
	private ResponsabileService responsabileService;
	@Autowired
	private CentroService centroService;

	@Autowired
	private StudenteValidator studentevalidator;




	// vai alla form dello studente 
	@GetMapping("/newStudente")
	public String goToForm(Model model) {
		model.addAttribute("studente", new Studente());
		return "Responsabilecentro/newStudente";
	}


	// gestisci l'inserimento dello studente
	@PostMapping("/studente")
	public String studenteSubmit(@Valid @ModelAttribute("studente") Studente studente,Authentication auth , HttpSession session , Model model,
			BindingResult bindingResult) {

		this.studentevalidator.validate(studente, bindingResult );
		if (this.studenteService.alreadyExists(studente)) {
			model.addAttribute("exists","studente gia presente");
			return "Responsabilecentro/newStudente";
		}else {


			if (!bindingResult.hasErrors()) {
				this.studenteService.save(studente);
				
				session.setAttribute("id", studente.getId());
				
				
				
				
				return "Responsabilecentro/studente";
			} else
				return "Responsabilecentro/newStudente";
		}
	}

	// vai alla pagina per aggiungere un corso(PORTA ALLA FORM COSRI)
	@GetMapping("/aggiungiStudenteCorso")
	public String goToaddCorso( Model model){
		model.addAttribute("corsi", this.corsoService.findAll());
		return "Responsabilecentro/aggiungiStudenteCorso";
	}

	@RequestMapping("/f") //aggiungo lo studente al corso e lo salvo nel db(menù a tedina del corso)
	public String salvaAllievoNuovoAttivita(@RequestParam("id") Long id, HttpSession session, Model model) 
	{
		Studente studente = this.studenteService.findById((Long)session.getAttribute("id"));

		if (studente!=null) {
			Corso att = this.corsoService.findById(id);
			att.getStudenti().add(studente);
			studente.getCorsi().add(att);
			this.corsoService.save(att);
			this.studenteService.save(studente);

			return "Responsabilecentro/inserito";
		}else


			return "Responsabilecentro/new Studente";

	}



}
