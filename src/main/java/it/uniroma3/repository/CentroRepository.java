package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;

public interface CentroRepository extends CrudRepository<Centro, Long>{
	
	
	

}
