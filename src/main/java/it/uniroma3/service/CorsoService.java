package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.repository.CorsoRepository;

@Transactional
@Service
public class CorsoService {
	
	@Autowired
	public CorsoRepository corsorepository;
	
	public Corso save(Corso c) {
		c.setNome(c.getNome().toUpperCase());
		return this.corsorepository.save(c);
	}
	
	public List<Corso> findByNome(String nome){
		return this.corsorepository.findByNome(nome);
	}
	public List<Corso> findByCentro(Centro centro){
		return this.corsorepository.findByCentro(centro);
	}
	
	public List<Corso> findAll(){
		return this.corsorepository.findAll();
	}
	
	public List<Corso> findByStudenti(Long id) {
		return this.corsorepository.findByStudenti_id(id);
	}

	public Corso findById(Long id) {
		Optional<Corso> s = this.corsorepository.findById(id);
		if(s.isPresent())
			return s.get();
		else
			return null;
	}
	


}
