package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.repository.CentroRepository;
import it.uniroma3.repository.ResponsabileRepository;

@Transactional
@Service
public class CentroService {
	@Autowired
	private CentroRepository CentroRepository; 
	@Autowired
	private ResponsabileRepository responsabileRepository; 
	
	public Centro save(Centro c) {
		return this.CentroRepository.save(c);
	}

	public List<Centro> findAll() {
		return (List<Centro>) this.CentroRepository.findAll();
	}
	
	
	public Centro findById(Long id) {
		Optional<Centro> c = this.CentroRepository.findById(id);
		if (c.isPresent()) 
			return c.get();
		else
			return null;
	}	

}
