package it.uniroma3.controller;


import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.controller.validator.CentroValidator;
import it.uniroma3.controller.validator.ResponsabileValidator;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Studente;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;
import it.uniroma3.service.StudenteService;

@Controller
public class CasoUC1 {
	@Autowired
	private CorsoService corsoService;
	
	@Autowired
	private CentroService centroService;
	
	@Autowired
	private StudenteService studenteService;
	
	
	@Autowired
	private ResponsabileService responsabileService;
	
	
	@Autowired
	private ResponsabileValidator responsabilevalidator;
	@Autowired
	private CentroValidator centrovalidator;
	
	
	
	//Da ResponsabilePannel -> pagina inserimento centro
		@RequestMapping("/addnewCentro")
		public String addCentro(Model model)
		{
			model.addAttribute("centro", new Centro());
			return "Responsabile/addCentro";
		}
		
		//Da pagina inserimento centro -> pagina inserimento responsabile
		@RequestMapping("/addCentro")
		public String addResponsabile(@Valid @ModelAttribute("centro") Centro centro, BindingResult br, Model model, HttpSession session)
		{
			centrovalidator.validate(centro, br);
			if(!br.hasErrors()) {
				session.setAttribute("centro", centro);
				model.addAttribute("responsabile", new Responsabile());
				return "Responsabile/addResponsabile";
			}
			else return "Responsabile/addCentro";
		}
		
		@RequestMapping("/registraResponsabile") 
		public String registroCentro(@Valid @ModelAttribute("responsabile") Responsabile responsabile, BindingResult br, Model model, HttpSession session)
		{
			responsabilevalidator.validate(responsabile, br);
			for(ObjectError e: br.getAllErrors()) System.out.println(e.toString());

			if(!br.hasErrors()) {
				Centro centro = (Centro)session.getAttribute("centro");
				saveInDatabase(centro,responsabile);
				return "Responsabile/ResponsabilePannel";
			}
			else return "Responsabile/addResponsabile";
		}	
	
		@Bean
		public BCryptPasswordEncoder bCryptPasswordEncoder() {
		    return new BCryptPasswordEncoder();
		}
		
//		salva il centro e registra il repsonsabile
		public void saveInDatabase(Centro c,Responsabile r)
		{
			BCryptPasswordEncoder b = bCryptPasswordEncoder();
			//1 : salvo centro senza responsabile (poichè r ancora non esiste nel db)
			centroService.save(c);
			//2 : salvo il responsabile dopo aver settato il centro
			c.setResponsabile(r);
			r.setCentro(c);
			r.setRuolo("RESPONSABILE");
			r.setPassword(b.encode(r.getPassword()));
			//3 : setto il responsabile (che ora esiste nel db) e aggiorno il centro
			responsabileService.save(r);
			centroService.save(c);
		}
	}


