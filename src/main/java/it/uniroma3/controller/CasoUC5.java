package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.model.Corso;
import it.uniroma3.model.Studente;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.StudenteService;

@Controller
public class CasoUC5 {
	//report attivita dello studente e del centro associato
	

	@Autowired
	private StudenteService studenteService;
	
	@Autowired
	private CorsoService corsoService;
	
	
	@RequestMapping ("/reportStudente")
	public String mostraLista(Model model) {
		model.addAttribute("studenti", this.studenteService.findAll());
		return "Responsabilecentro/ReportStudente";
	}
	
	
	
	

}
