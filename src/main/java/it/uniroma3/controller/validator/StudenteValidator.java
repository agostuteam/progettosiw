package it.uniroma3.controller.validator;



import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Studente;

@Component
public class StudenteValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Studente.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "required","campo obbligatorio!");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cognome", "required","campo obbligatorio!");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required","campo obbligatorio!");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dataNascita", "required","campo obbligatorio");
		
		
        
	}

	
}
