package it.uniroma3.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import it.uniroma3.service.ResponsabileService;

@Controller
public class HomeController {
	@Autowired
	private ResponsabileService service;
	
	@RequestMapping("/")
	public String login(Model model,Authentication auth)
	{
		//Se voglio andare alla homePage, ma sono già loggato come responsabile 
		if(hasRole("RESPONSABILE")) {
			model.addAttribute("log","Benvenuto , "+auth.getName());
			
			return "/Responsabilecentro/adminPanel";
		}
		else 
		{
			//Se voglio andare alla homePage, ma sono già loggato come direttore
			if(hasRole("DIRETTORE")) {
				model.addAttribute("log",auth.getName());
				return "/Responsabile/ResponsabilePannel";
			}
		    else 
		    {
		    	//Se voglio andare alla homePage, e non sono loggato
		    	return "index";
		    }
		}
	}
	
	protected boolean hasRole(String role) {
        // get security context from thread local
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null)
            return false;

        Authentication authentication = context.getAuthentication();
        if (authentication == null)
            return false;

        for (GrantedAuthority auth : authentication.getAuthorities()) {
            if (role.equals(auth.getAuthority()))
                return true;
        }
        return false;
    }
	

}
