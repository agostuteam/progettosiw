package it.uniroma3.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import it.uniroma3.controller.validator.CorsoValidator;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;
import it.uniroma3.service.StudenteService;


@Controller
public class CasoUC4 {
	
	@Autowired
	private CorsoValidator corsovalidator;

	@Autowired
	private StudenteService studenteService;
	
	@Autowired
	private ResponsabileService reposabileService;
	
	@Autowired
	private CorsoService corsoService;
	
	@Autowired
	private CentroService centroService;
	


	
	
	//da pannello Responsabile centro ---> aggiunta di una nuovo corso
	@GetMapping("/newCorso")
	public String goToCorsoForm(Model model) {
		model.addAttribute("corso", new Corso());
		return "Responsabilecentro/newCorso";
	}
	
	//salva il corso nel database---risponde form corso
	@PostMapping("/corso")
    public String corsoSubmit(@Valid @ModelAttribute("corso") Corso corso, HttpSession session , Model model,
            BindingResult bindingResult, Authentication auth) {
 
		this.corsovalidator.validate(corso, bindingResult);
        if (!bindingResult.hasErrors()) {
        	//Ottengo centro associato al responsabile
        	Centro centro = this.reposabileService.findByEmail(auth.getName()).get(0).getCentro();

        	 putIntoDatabase(centro, corso);
        	 model.addAttribute("corso",corso);
        	 model.addAttribute("studenti",corso.getStudenti());
          
            return "Responsabilecentro/corso";
        	
        
        } else
            return "Responsabilecentro/newCorso";
    }
	
	
	
	
	//salva il corso nel centro e lo aggiunge ai corsi del centro e risalva il centro
	public void putIntoDatabase(Centro centro,Corso corso)
	{
		//1 : salvo corso (poichè il centro già si trova nel db)
		corso.setCentro(centro);
		corsoService.save(corso);
		//2 : aggiorno centro
		centro.getCorsi().add(corso);
		centroService.save(centro);
	}

}
