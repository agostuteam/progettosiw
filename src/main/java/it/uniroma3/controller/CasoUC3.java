package it.uniroma3.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Studente;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;
import it.uniroma3.service.StudenteService;

@Controller
public class CasoUC3 {
	

	@Autowired
	private StudenteService studenteService;
	
	@Autowired
	private CorsoService corsoService;
	
	@Autowired
	private CentroService centroService;

	@Autowired
	private ResponsabileService responsabileService;
	
	
	// vai a cercastudente
		@GetMapping("/cercaStudente")
		public String goToSearch(Model model) {
			model.addAttribute("studente", new Studente());
			return "Responsabilecentro/cercaStudente";
		}
		
		
		// vai a mostra listaStudenti
		@GetMapping("/listaStudenti")
		public String mostraListaStudenti(HttpSession session ,Authentication auth ,Model model) {
//		Long id =(Long)session.getAttribute("responsabileid");
		
			model.addAttribute("studenti",this.studenteService.findAll());
			return "Responsabilecentro/listaStudenti";
		}
		
		
		// vai a listaCorsi
		
		@GetMapping("/mostraCorsi")
		public String mostraListaCorsi(Model model) {
			model.addAttribute("corsi", this.corsoService.findAll());
			return "Responsabilecentro/listaCorsi";
		}
		
		// mostra i dati di un corso
		@RequestMapping(value ="/corso/{id}", method = RequestMethod.GET)
		public String getCorso(@PathVariable("id") Long id, Model model) {
			List<Studente> iscritti =  this.studenteService.findByCorso(id);
			model.addAttribute("corso", this.corsoService.findById(id));
			model.addAttribute("studenti", iscritti);
			model.addAttribute("numero", iscritti.size());
			return "Responsabilecentro/corso";

		}
	
		
		// gestisci la ricerca dello studente per convenzione la ricerca è per cognome
		@GetMapping("/ricerca")
		public String studenteSearch(@ModelAttribute("studente") Studente studente, Model model) {	
			List<Studente> studenti = this.studenteService.findbyCognome(studente.getCognome().toUpperCase());
			if( studenti.size()!=0){
			model.addAttribute("studenti", studenti);	
			return "Responsabilecentro/listaStudenti";
			}else {
				model.addAttribute("unserch","utente non trovato");
				return "Responsabilecentro/cercaStudente";
			}
			}
		   // mostra i dati di uno studente
	    @RequestMapping(value = "/studente/{id}", method = RequestMethod.GET)
	    public String getCustomer(@PathVariable("id") Long id, Model model,HttpSession session) {
	    	
	    	List<Corso> corsi = this.corsoService.findByStudenti(id);
	    	Studente studente = this.studenteService.findById(id);
	    	session.setAttribute("id", id);
	        model.addAttribute("studente", studente);
	        model.addAttribute("corsi", corsi);
	        return "Responsabilecentro/studente";
	    }
	
	

}
