package model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Allievo {

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Long id ;

    @Column(nullable = false)
    private String nome ;
    
    @Column(nullable = false)
    private String cognome ;
    
    @Column
    private String telefono ;
    
    @Column
    private String email ;
    
    @Temporal(TemporalType.DATE)
    private Date datanascita ;
    
    @Column(nullable = false)
    private String luogonascita ;
   
    @ManyToMany
    private List<Attivita> attivita;

	public Allievo(Long id, String nome, String cognome, String telefono, String email, Date datanascita,
			String luogonascita) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
		this.email = email;
		this.datanascita = datanascita;
		this.luogonascita = luogonascita;
	}
    public Allievo() {
    	
    }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDatanascita() {
		return datanascita;
	}
	public void setDatanascita(Date datanascita) {
		this.datanascita = datanascita;
	}
	public String getLuogonascita() {
		return luogonascita;
	}
	public void setLuogonascita(String luogonascita) {
		this.luogonascita = luogonascita;
	}
    
    













}
