package model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Attivita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	
	@Column(nullable = false )
	private String nome ;
	
	@Temporal(TemporalType.DATE)
	private Date data ;
	
	@Temporal(TemporalType.DATE)
	private Date ora;
	
	@ManyToOne
	private Centro centro ;
	
	@ManyToMany(mappedBy = "attivita")
	private List<Allievo> allievi;
	
	@ManyToOne
	private Docente docente;

	public Attivita(Long id, String nome, Date data, Date ora) {
		super();
		this.id = id;
		this.nome = nome;
		this.data = data;
		this.ora = ora;
	}
	
	public Attivita() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getOra() {
		return ora;
	}

	public void setOra(Date ora) {
		this.ora = ora;
	}
	
	
 
	
	
	
	
}
