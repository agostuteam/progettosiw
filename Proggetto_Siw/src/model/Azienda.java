package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Azienda {
	@Id
	@GeneratedValue (strategy= GenerationType.AUTO)
	private Long id;
	
	

	@Column(nullable = false)
	private String nomeazienda ;


	@OneToMany
	@JoinColumn(name = "azienda_id")
	private List<Centro> centri;

	@OneToOne(mappedBy = "azienda")
	private Responsabile responsabile;

	@OneToMany
	@JoinColumn(name = "azienda_id")
	private List<Allievo> allievi;

	public Azienda(String nomeazienda) {

		this.nomeazienda = nomeazienda;
	}
	public Azienda() {


	}
	public String getNomeazienda() {
		return nomeazienda;
	}
	public void setNomeazienda(String nomeazienda) {
		this.nomeazienda = nomeazienda;
	}
	






}
