package repository;

import java.util.List;

public interface GenericsRepository<T> {
	
	public void save (T obj);
	
	public T findPrimaryKey(Long id );
	
	public List<T> findAll();
	
	public void update (T obj);
	
	public void delete (T obj);

}
