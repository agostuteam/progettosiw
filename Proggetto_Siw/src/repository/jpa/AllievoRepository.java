package repository.jpa;

import java.util.List;

import javax.persistence.EntityManager;

import model.Allievo;
import repository.GenericsRepository;

public class AllievoRepository implements GenericsRepository<Allievo> {
	
	private EntityManager em;
	
	

	public AllievoRepository(EntityManager em) {
		this.em = em;
	}

	@Override
	public void save(Allievo allievo) {
		em.persist(allievo);
	}

	@Override
	public Allievo findPrimaryKey(Long id) {
		return (Allievo) em.find(Allievo.class, id);
	}

	@Override
	public List<Allievo> findAll() {
		return em.createNamedQuery("SELECT a FROM Allievo a", Allievo.class).getResultList();
	}

	@Override
	public void update(Allievo allievo) {
		em.merge(allievo);
	}

	@Override
	public void delete(Allievo allievo) {
    em.remove(allievo);
		
	}
	

}
